<?php include "php/header.php";?>

<?php include "php/nav_bar.php";?>

<section class="banner_main">
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
              <div class="animated_word">
                <span>D</span><span>A</span><span>N</span><span>I</span><span>E</span><span>L</span><span>&nbsp;</span>
                <span>M</span><span>A</span><span>N</span><span>D</span><span>E</span><span>L</span><span>L</span><span>I</span>
              </div>
              <script type="text/javascript" src="/scripts/name_animation.js"></script>
              <br></br>
              <div class='console-container' style="color:#4E9F3D">
                <span class="before_terminal">[</span>
                <span id='text'></span>
                <div class='console-underscore' id='console' style="color:white">&#95;</div>
                <span class="after_terminal">]</span>
              </div>
              <script type="text/javascript" src="/scripts/terminal_animation.js"></script>
              <br></br>
              <div class='contact-us-content header-text'>
              <p>
                I'm a Software Engineer based out of East Providence, RI.<br>
                I attended the University of Rhode Island and graduated in May 2019 with a degree in Computer Engineering.<br>
                For the past 4 Years I have worked professionally as Full Stack Software Developer.
                <br><br>
                In 2023, I'm Starting a Web Design and Hosting Agency, <a href="https://WebWiseTechnology.com/">WebWise Technology</a>.<br>
                I'm learning new skills everyday and am hosting to expand my career and obtain a full time front end position while working on WebWise Technology on the side.
                <br>
                I'm currently looking for exciting new career opportunities that fit my experience and skillset.
              </p>
                <div class="more-info" id="contact">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="info-item">
                        <i class="fa fa-phone"></i>
                        <h4><a href="tel:14015722377">+1 (401) 572-2377</a></h4>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="info-item">
                        <i class="fa fa-envelope"></i>
                        <h4><a href="mailto:DannyMandelli@gmail.com">DannyMandelli@gmail.com</a></h4>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
        <div class="col-md-4">
          <div class="ban_img">
              <figure><img src="/photos/dmandelli_transparent.png" alt="transparent photo of Daniel"></figure>
          </div>
        </div>
    </div>
  </div>
</section>
<!-- First Container -->
<!-- <div class="container-fluid text-center">
  <div class="item1">
    <div class="animated_word">
      <span>D</span><span>A</span><span>N</span><span>I</span><span>E</span><span>L</span><span>&nbsp;</span>
      <span>M</span><span>A</span><span>N</span><span>D</span><span>E</span><span>L</span><span>L</span><span>I</span>
    </div>
    <script type="text/javascript" src="/scripts/name_animation.js"></script>
    <br></br>
    <img src="/photos/dmandelli_transparent.png" class="img-responsive" alt="transparent photo of Daniel" style="display:inline" width="300" height="600">
    <br></br>
  </div>
  <div class="item2">
    <br></br>
    <div class='console-container' style="color:#4E9F3D">
      <span class="before_terminal">[</span>
      <span id='text'></span>
      <div class='console-underscore' id='console' style="color:white">&#95;</div>
      <span class="after_terminal">]</span>
    </div>
    <script type="text/javascript" src="/scripts/terminal_animation.js"></script>
    <br></br>
    <img src="/photos/Daniel_Mandelli.png" class="img-responsive" alt="Headshot" style="display:inline; margin:auto" width="400" height="200">
    <br></br>
    <br></br>
    <br></br>
    <br></br>
    <p>
      Welcome! My name is Daniel Mandelli, a Software Engineer based out of East Providence, RI.
      I attended the University of Rhode Island and graduated in May 2019 with a degree in Computer Engineering.
      For the past 4 Years I have worked professionally as Full Stack Software Developer.
      I hope to gain more experience in all things web development as I continue to work on side projects.
      Currently I am working on an eccomerce site using React Next.js and Sanity.
      I'm currently looking for exciting new career opportunities that fits my experience.

    </p>
  </div>
</div> -->

<?php include "php/footer.php";?>
