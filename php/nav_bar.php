
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/style.css" >
<link rel="stylesheet" href="/css/template.css" >

<nav id="navbar-placeholder" class="navbar navbar-expand-md navbar-dark bg-dark">
  <div class="container">
    <a class="navbar-brand brand_name" href="/"><h2>Daniel Mandelli</h2></a>
    <div class="social-part">
      <a href="https://www.linkedin.com/in/daniel-mandelli/" target="_blank" rel="noreferrer noopener"><i class="fa fa-linkedin"></i></a>
      <a href="https://www.facebook.com/danny.mandelli" target="_blank" rel="noreferrer noopener"><i class="fa fa-facebook"></i></a>
    </div>
    <button class="navbar-toggler" type="button" id="navbar-toggler" data-toggle="collapse"
    data-target="#navbarResponsive" aria-controls="navbarResponsive"
    aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon active"></span>
    </button>
    <div class="nav collapse navbar-collapse" id="navbarResponsive">
      <ul class="nav navbar-nav mr-auto mb-2 mb-lg-0" id="navbar_list">
        <li class="has-sub">
          <a class="nav-link" href="/education">Education</a>
              <ul class="sub-menu">
                  <li><a href="/education#university">University</a></li>
                  <li><a href="/education#capstone">Capstone Project</a></li>
              </ul>
        </li>
        <li class="has-sub">
          <a class="nav-link" href="/experience">Experience</a>
              <ul class="sub-menu">
                  <li><a href="/experience#skills">Skills and Technology</a></li>
                  <li><a href="/experience#work_experience">Work Experience</a></li>
                  <li><a href="/experience#projects">Projects</a></li>
              </ul>
        </li>
        <li class="nav-item menu_dropdown">
          <a class="nav-link" href="/files/Daniel_Mandelli_Resume.pdf" target="_blank" rel="noreferrer noopener" >Resume</a>
        </li>
      </ul>
      <ul class="nav navbar-nav ml-auto mr-auto mb-2 mb-lg-0">
        <li class= "nav-item dropdown ">
          <a class="btn btn-secondary dropdown-toggle" href="#" role="button" aria-controls="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true">
            Contact Me
          </a>
          <div class="dropdown-menu dropdown-menu-right" id="dropdownMenuButton">
            <a class="dropdown-item" href="mailto:Dannymandelli@gmail.com" target="_blank" rel="noreferrer noopener"><i class="fa fa-solid fa-envelope"></i> <b>Dannymandelli@gmail.com</b></a>
            <a class="dropdown-item" href="tel:14015722377" target="_blank" rel="noreferrer noopener"><i class="fa fa-phone"></i> <b>Call me at 401-572-2377 </b></a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
